const ws = new WebSocket('wss://websocket2022-demo-dot-webstie2022.de.r.appspot.com/socket.io/')
const catchError = (close) => {
    console.log('客戶端 close', close)
}

ws.addEventListener('close', catchError);

ws.onopen = () => {
    console.log('連接 Service 開始')
}

ws.onclose = () => {
    console.log('連接 Service 結束')
}

ws.onmessage = (event) => {
    console.log('client:message', event)
}