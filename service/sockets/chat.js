const WebSocket = require("ws");
const { Server: SocketServer } = WebSocket;
const { useLogs } = require("../utils/logger");
const { setLog } = useLogs();
const { useSystemMessage } = require("../utils/chat-message");

const useHeartbeat = (ws) => {
  const beat = () => {
    ws.isAlive = true;
  };
  const ping = () => {
    if (ws.isAlive === false) return ws.terminate();

    ws.isAlive = false;
    ws.ping();
  };
  const eachPing = (list) => list.forEach(ping);

  const interval = (fn, s = 30) => setInterval(() => {
    fn()
  }, 1000 * s);

  const pong = () => {
    beat();
    ws.on("pong", beat);
  }

  return { beat, ping, eachPing, pong, interval };
};

const useWs = (WsOption) => {
  const wss = new SocketServer(WsOption);

  wss.on("connection", (ws, request) => {
    let log = "有 Client 連接";
    setLog(log);

    const { pong, ping, interval } = useHeartbeat(ws);
    let intervalTimer = interval(ping, 60);

    pong();

    ws.on("message", (message, isBinary) => {
      formatMsg = isBinary ? message : message.toString().replace(/<script>.*<\/script>/,'');
      log = `有 Client 傳資料 : ${isBinary? 'isBinary':formatMsg}`;
      setLog(log);

      const systemMsg = useSystemMessage(formatMsg)
      systemMsg.forUser && systemMsg.forUser.forEach((msg) => ws.send(msg));

      wss.clients.forEach((client) => {
        // 自己以外的人收到訊息
        if (client !== ws && client.readyState === WebSocket.OPEN) {
          if (systemMsg.forClients) {
            systemMsg.forClients.forEach((msg)=> client.send(msg, { binary: isBinary }))
          } else {
            client.send(message, { binary: isBinary });
          }
        }
      });
    });

    ws.on("close", (close) => {
      clearInterval(intervalTimer);
      intervalTimer = null;
      log = `有 Client 連接結束: code ${close}`;
      setLog(log);
      
    });
    ws.on("error", (error) => {
      clearInterval(intervalTimer);
      intervalTimer = null;
      log = `有 Client 連接 Error ${error}`;
      setLog(log);
    });
  });

  wss.on("close", (close) => {
    setLog(`服務器WWS:close ${close}`);
    console.log("服務器WWS:close", error);
  });

  wss.on("error", (error) => {
    setLog(`服務器WWS:error ${error}`);
    console.error("服務器WWS:error", error);
  });

  return wss;
};

module.exports = useWs;
