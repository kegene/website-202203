const express = require("express");
// utils
const useWss = require('./sockets/chat')
const {
  handlerStatusError,
  handlerRouteError,
} = require("./utils/handler-status");

// cors
const cors = require("cors");
const { corsOptions } = require("./config/cors");

// router
const userRouter = require("./api/user");
const loggerRouter = require("./api/logger");

const app = express();
const server = require("http").Server(app);
const PORT = process.env.PORT || 8080;

app.use(express.static(__dirname + "/public"));
app.disable("x-powered-by");

app.use(cors(corsOptions));
app.use("/user", userRouter);
app.use("/logger", loggerRouter);
app.use(handlerStatusError);


app.get("/", (req, res) => {
  res.send('hello world')
});
app.get("/see-log", (req, res) => {
    res.sendFile(__dirname + "/public/" + "log.html", "utf8");
});
app.get('/favicon.ico', function(req, res) { 
  res.sendStatus(204); 
});

app.use(handlerRouteError);

useWss({server})

server.listen(PORT, () => console.log(`Listening on ${PORT}`));
