const corsOptions = {
    origin: [/localhost/, /kegene\.gitlab\.io/],
    optionsSuccessStatus: 200,
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
    allowedHeaders: ['Content-Type', 'Authorization'],
}

module.exports = {
    corsOptions
}