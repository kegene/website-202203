let logs = []
const useLogs = (ary = []) => {
    logs = ary;

    const dev = (fn) => {
        return (...arg) => {
            if (process.env.NODE_ENV === 'development') {
                fn(arg)
            }
        }
    }

    const setLog = dev((log) => {
        logs.push({ log, id: logs.length, time: new Date().toISOString()})

        console.log('setLog', log);
    });
    const getLog = dev((log) => logs.filter((l) => l.id === log.id));
    return {
        logs,
        setLog,
        getLog,
    }
}

module.exports = {
    useLogs
}