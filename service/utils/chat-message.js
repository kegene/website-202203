const MSG_TYPE = {
    SYSTEM: "system",
    WARN: "warn",
    ERROR: "error",
    IMPORTANT:"important"
}
/**
 * 
 * @param {string} type 'system' | 'warn' | 'error' | 'important'; 
 * @param {string} message 
 */
 const createSystemMsg = (type, message) => {
    return {
        user: {uuid:'0'},
        message: {
            content: message,
            type: type
        }
    }
}


const exitMessage = (user) => {
    return createSystemMsg(MSG_TYPE.SYSTEM, `${user} 已退出`)
}
const joinMessage = (user) => {
    return createSystemMsg(MSG_TYPE.SYSTEM, `${user} 已加入`)
}
const fileServiceCloseMessage = (user) => {
    return createSystemMsg(MSG_TYPE.WARN, `此訊息未發送，因資源有限，無法提供真實傳輸文件功能`)
}
const errorMessage = (msg = `出現錯誤，請耐心等候修復`) => {
    return createSystemMsg(MSG_TYPE.ERROR, msg)
}
const importantMessage = (user, msg) => {
    return createSystemMsg(MSG_TYPE.WARN, `Hi, ${user}: ${msg}`)
}

const regExps = {
    messageType: /"message":{.*[^{]?"type":"(\w+)"/,
    userName: /"user":{.*[^{]?"name":"([\u4e00-\u9fa5\w]+)"/,
}

/**
 * 取得 { "ke"y: "value" } 形式的 json value
 * @param {string} json 
 * @param {keyof typeof regExps} key 
 * @returns {string} value
 */
const getJsonValue = (json, key) => {
    if (!(typeof json === 'string')) return;
    const res = json.match(regExps[key]);
    return res && res.length ? res[1] : false;
}
const useSystemMessage = (msg) => {
    const messageType = getJsonValue(msg, 'messageType')
    if (messageType === 'join') return getJoinMessage(msg)
    if (messageType === 'exit') return getExitMessage(msg)

    return {};
}
const getJoinMessage = (msg) => {
    const userName = getJsonValue(msg, 'userName')
    if (!getJsonValue) return {}
    return {
        forClients: [
            JSON.stringify(joinMessage(userName)),
        ],
        forUser: [
            JSON.stringify(joinMessage(userName)),
            JSON.stringify(importantMessage(userName,`
            🌟有時候可能因伺服器在休眠，初次訪問無回應，請待30秒後重新進入此畫面（應該就好了）。
            🌟只有一個人話，可以開雙視窗跟自己聊天
            🌟雖然不會保留任何聊天訊息，但請別隨意傳遞個人重要訊息`)),
        ]
    }
}

const getExitMessage = (msg) => {
    const userName = getJsonValue(msg, 'userName')
    if (!getJsonValue) return {}
    return {
        forClients: [
            JSON.stringify(exitMessage(userName))
        ]
    }
}
module.exports = {
    createSystemMsg,
    exitMessage,
    joinMessage,
    fileServiceCloseMessage,
    errorMessage,
    importantMessage,
    getJsonValue,
    useSystemMessage
}