const express = require('express');

const {useLogs} = require('../utils/logger')

const router = express.Router();

const { logs } = useLogs();

router.get('/', (req, res, _) => {
    res.send(logs);
});

module.exports = router