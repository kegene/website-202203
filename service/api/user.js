const express = require('express');
const router = express.Router();

const { v4: uuidv4 } = require('uuid');


router.get('/fake-id', (req, res, _) => {
    res.json({
        status: 0,
        message: 'ok',
        data: uuidv4()
    });
});

module.exports = router