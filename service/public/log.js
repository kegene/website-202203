const useTimeout = (callback, speed = 1) => {
    let timer = null;
    let _speed = 1000 * speed;

     const stop = () => {
        console.log('call:stop')
        clearTimeout(timer);
        timer = null;
     }
     const run = async (speed = _speed) => {
        timer = setTimeout(async () => {
            await callback({stop, run})
        }, speed)
     }

     /**
      * @param { number } speedMultiple 速度的倍數
     */
     const handlerSpeed = (speedMultiple) => {
        stop()
        run(_speed / parseInt(speedMultiple))
     }
    return {
        run,
        stop,
        handlerSpeed,
    }
}
const useLogs = (ary = []) => {
    const logs = {
        value: ary
    }
    const setLogs = (data) => logs.value = [...data];
    const getLog = (log) => logs.value.filter((l) => l.id === log.id);
    const getLastLog = () => logs.value.length && logs.value[logs.value.length - 1]
    const getLogs = () => logs.value;
    return {
        logs,
        setLogs,
        getLogs,
        getLog,
        getLastLog
    }
}
export {
    useTimeout,
    useLogs
}