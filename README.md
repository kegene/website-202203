# 運用 WebSocket 實作 即時聊天

`vue3` `webSocket` `typescript` `axios`

| dark                           | light                          |
| ------------------------------ | ------------------------------ |
| ![](./readme-assets/000-b.jpg) | ![](./readme-assets/000-w.jpg) |
| ![](./readme-assets/001-b.jpg) | ![](./readme-assets/001-w.jpg) |
| ![](./readme-assets/002-b.jpg) | ![](./readme-assets/002-w.jpg) |
## 簡介

提供多個用戶即時聊天。[DEMO | gitlab.io](https://kegene.gitlab.io/website-202203)

## 項目資訊

**Front-end:**
- Vue 3
- Pinia
- Tailwind 3
- Vite

**Back-end:**
- Nodejs
- Express

**運行主機:**
免費三個月的 Google App Engine 彈性模式 (其他設定皆為預設)

（不知道為什麼用標準模式下，websocket連線無法成功，會顯示101驚嘆號，所以改用彈性模式）

## 功能開發

- [X] 提供入場退場及注意說明的系統通知
- [X] 提供圖片上傳 (但由於伺服器流量受限，不會真的將文件上傳，僅提供本地預覽)
- [ ] 提供內建表情符號
