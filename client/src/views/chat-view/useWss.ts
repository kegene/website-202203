type WebSocketAddListenerParams = Parameters<WebSocket['addEventListener']>;

export class ChatWs extends WebSocket {
  public clientWaitTimer: number | null = null;
  public moreListeners: WebSocketAddListenerParams[] = []

  public addSimpleAutoListener: WebSocket['addEventListener'] = (...arg: WebSocketAddListenerParams) => {
    this.moreListeners.push(arg);
    this.addEventListener(...arg);
  }

  constructor(...arg: ConstructorParameters<typeof WebSocket>) {
    super(...arg)
  }

  heartbeat() {
    this.offHeartbeat();

    this.clientWaitTimer = setTimeout(() => {
      this.close(4000, '客戶或伺服器無回應');
    }, 1000 * 60 * 60);
  }

  offHeartbeat() {
    typeof this.clientWaitTimer === 'number' && clearTimeout(this.clientWaitTimer);
    this.clientWaitTimer = null
  }

  public customOpen(event: Event) {
    console.log('open', event)
    this.heartbeat();
  }
  public customClose(close: CloseEvent) {
    console.log('close', close)
    this.offHeartbeat()
  }
  public customMessage(event: MessageEvent) {
    this.heartbeat()
  }
  public openHeartbeat() {
    this.addEventListener('open',this.customOpen)
    this.addEventListener('close',this.customClose)
    this.addEventListener('message',this.customMessage)
  }
  public closure(code: number = 4001, msg: string = '客戶退出連線頁面 closure:close') {
    this.removeEventListener('open',this.customOpen)
    this.removeEventListener('close',this.customClose)
    this.removeEventListener('message',this.customMessage)

    for (const arg of this.moreListeners) {
      this.removeEventListener(...arg)
    }

    this.close(code, msg)
  }
}
