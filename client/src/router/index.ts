import { createRouter, createWebHistory } from 'vue-router'
import Login from '@/views/UserLogin.vue'
import useUserStore from '@/store/UserStore'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/chat',
      name: 'home',
      component: ()=> import('@/views/chat-view/ChatView.vue'),
      beforeEnter: (to,from,next) => {
        const userStore = useUserStore();
        if (!userStore.user.uuid) return next(from);
        next()
      }
    },
  ]
})

export default router
