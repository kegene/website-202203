import type { DarkTheme } from './styles/hendler-theme'
import type { InjectionKey } from 'vue'

export const DarkThemeKeys: InjectionKey<DarkTheme> = Symbol('dark')