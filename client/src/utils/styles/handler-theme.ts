import type {UseDarkTheme} from './hendler-theme'
import { computed, ref } from 'vue';
import {getPrefersColorScheme} from './get-styles';

const useDarkTheme: UseDarkTheme = () => {
  const dark = ref(false);
  const isDark = computed(() => dark.value);

  const setDark = (isDark: boolean) => {
    const key = isDark ? 'add' : 'remove';

    dark.value = isDark;
    document.documentElement.classList[key]('dark')
  }
  const triggerDark = () => {
    dark.value = !dark.value
  }
  setDark(getPrefersColorScheme())

  return {
    isDark,
    setDark,
    triggerDark
  }
}

export default useDarkTheme