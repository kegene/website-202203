import type {ComputedRef} from 'vue';

export interface DarkTheme {
    isDark: ComputedRef<boolean>;
    setDark: (isDark: boolean) => void
    triggerDark: () => void
  }
  
export type UseDarkTheme = () => DarkTheme
  