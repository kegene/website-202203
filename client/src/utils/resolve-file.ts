const toBase64 = (file: File): Promise<FileReader['result']> => new Promise((resolve, reject) => {
  const reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = () => resolve(reader.result);
  reader.onerror = error => reject(error);
});
const toBlob = (data: any, options: BlobPropertyBag = {type: 'application/json'}) => new Blob([JSON.stringify(data)], options)

const filePhotoTypes = [
  "image/bmp",
  "image/gif",
  "image/jpeg",
  "image/png",
  "image/tiff",
  "image/webp",
];

const checkPhoto = (file: File) => {
  return !filePhotoTypes.includes(file.type)
}

export {
  toBase64,
  toBlob,
  filePhotoTypes,
  checkPhoto
}