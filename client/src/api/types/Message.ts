export type IMessageClientType = "file" | "text" | "image" | "sticker";
export type IMessageSystemType =
  | "system"
  | "warn"
  | "error"
  | "important"
  | "join"
  | "exit";

export type IMessageType = IMessageClientType | IMessageSystemType;
export enum Level {
  system = 0,
  normal = 1,
  manager = 2,
}
export interface IUser<T> {
  uuid: string;
  name: string;
  avatarColor: T | string;
  level: Level;
}
export interface IMsg<T extends IMessageType> {
  type: T;
  content: any
}

export interface IMessage<U,M extends IMessageType> {
  user: IUser<U>;
  message: IMsg<M>;
}