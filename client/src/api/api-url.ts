
const getURL = (host: string) => location.origin.replace(/(\/\/)(.*)/, `$1${host}`);
export const API_URL = {
  BASE: getURL(import.meta.env.VITE_BASE_API),
  CHAR: getURL(import.meta.env.VITE_BASE_API + '/socket.io'),
  GET_USER_ID: getURL(import.meta.env.VITE_BASE_API + '/user/fake-id'),
}