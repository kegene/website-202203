import { createApp } from 'vue'
import { createPinia } from 'pinia'
import App from '@/App.vue'
import router from '@/router'

import useDarkTheme from '@/utils/styles/handler-theme'
import {DarkThemeKeys} from '@/utils/provide-keys'

import ToastPlugin from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';

import '@/assets/main.css'
import '@/assets/tailwind.css'

const app = createApp(App)
const pinia = createPinia()

app.use(router)
app.use(pinia)
app.use(ToastPlugin)

app.provide(DarkThemeKeys, useDarkTheme())

app.mount('#app')
