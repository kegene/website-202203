import type {AvatarColorKey} from './types'
type AvatarColorValue = `bg-${AvatarColorKey}-500 text-stone-${'100'|'900'}`;

type AvatarColor = {
  [key: string | AvatarColorKey]: AvatarColorValue;
}

export const AVATAR_COLOR: AvatarColor = {
  slate: 'bg-slate-500 text-stone-100',
  gray: 'bg-gray-500 text-stone-100',
  zinc: 'bg-zinc-500 text-stone-100',
  neutral: 'bg-neutral-500 text-stone-100',
  stone: 'bg-stone-500 text-stone-100',
  red: 'bg-red-500 text-stone-100',
  orange: 'bg-orange-500 text-stone-100',
  amber: 'bg-amber-500 text-stone-900',
  yellow: 'bg-yellow-500 text-stone-900',
  lime: 'bg-lime-500 text-stone-900',
  green: 'bg-green-500 text-stone-900',
  emerald: 'bg-emerald-500 text-stone-900',
  teal: 'bg-teal-500 text-stone-900',
  cyan: 'bg-cyan-500 text-stone-900',
  sky: 'bg-sky-500 text-stone-900',
  blue: 'bg-blue-500 text-stone-100',
  indigo: 'bg-indigo-500 text-stone-100',
  violet: 'bg-violet-500 text-stone-100',
  pink: 'bg-pink-500 text-stone-100',
  rose: 'bg-rose-500 text-stone-100',
}

export const randomColor = () =>  {
  const values = Object.values(AVATAR_COLOR);
  return values[~~(Math.random() * values.length)]
}
export const randomColorKey = (): AvatarColorKey => {
  const keys = Object.keys(AVATAR_COLOR) as AvatarColorKey[];
  return keys[~~(Math.random() * keys.length)]
}