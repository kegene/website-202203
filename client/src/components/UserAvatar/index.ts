import UserAvatar from "./UserAvatar.vue";
export type { Props as  UserAvatarProps} from "./UserAvatar.vue";
export type {AvatarColorKey} from './types';
export {randomColorKey} from './utils';
export default UserAvatar;