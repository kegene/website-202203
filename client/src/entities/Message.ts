import type {IUser, IMsg, IMessageType} from '@/api/types/Message';
export class Message<Avatar> {
  public user: IUser<Avatar> = {
    uuid: '',
    name: '',
    avatarColor: '',
    level: 1,
  }
  public message: IMsg<IMessageType> = {
    type: 'text',
    content: ''
  }

  constructor(user: IUser<Avatar>) {
    Object.assign(this.user, user)
  }
  
  setMessage<T extends IMessageType>({content, type}: IMsg<T>) {
    this.message = {content, type}
  }
  resetMessage() {
    this.message = {content:'', type: 'text'}
  }
  get() {
    return {
      message: this.message,
      user: this.user
    }
  }
}
