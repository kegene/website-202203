import { defineStore } from 'pinia';

import type {IUser, IMsg, IMessageType} from '@/api/types/Message';

type IUserInfo = IUser<string>;
type IMsgInfo = IMsg<IMessageType>;

interface UserStore {
  user: IUserInfo;
  message: IMsgInfo;
}

const useUserStore = defineStore('user', {
  state: (): UserStore => ({ 
    user: {
      uuid: '',
      name: '',
      level: -1,
      avatarColor: '',
    },
    message: {
      type: 'text',
      content: ''
    },
  }),
  getters: {
  },
  actions: {
    setUserInfo(user: IUserInfo) {
      this.user = user;
    },
  },
})

export default useUserStore