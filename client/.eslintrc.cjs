/* eslint-env node */
require('@rushstack/eslint-patch/modern-module-resolution')
module.exports = {
  root: true,
  'extends': [
    'plugin:vue/vue3-essential',
    'eslint:recommended',
    '@vue/eslint-config-typescript'
  ],
  parserOptions: {
    ecmaVersion: 'latest',
    parser: "@typescript-eslint/parser",
  },
  rules: {
    'no-undef': "off",
    "indent": ["error", 2],
    // 空格行數
    "no-multiple-empty-lines": ["error", { "max": 1, "maxEOF": 1 }],
    // 縮排
    "vue/html-indent": ["error", 2, {
      "attribute": 2,
      "baseIndent": 2,
      "closeBracket": 0,
      "alignAttributesVertically": true,
    }],
    // 限制每行屬性數量，超過則換行
    'vue/max-attributes-per-line': ['error', {
      singleline: {
        max: 3
      },
      multiline: {
        max: 1
      }
    }],
    // 關閉符號是否換行。
    "vue/html-closing-bracket-newline": ["error", {
      "singleline": "never", // 單行屬性：不換行
      "multiline": "always" // 多行屬性：換行
    }]
  }
}
