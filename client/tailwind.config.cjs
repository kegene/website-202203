/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: 'class',
  content: [
    "./src/**/*.{vue,css,ts,tsx}",
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}
